<?php

return [
    [
        'text'     => 'Dashboard details',
        'icon'     => 'fa-info',
        'subitems' => [
            [
                'link'         => 'dashboard/tech/installation',
                'text'         => 'Installation',
                'icon'         => 'far fa-circle',
                'active_rules' => [
                    'urls' => [
                        'dashboard/tech/installation'
                    ],
                ],
            ],
            [
                'link'         => 'dashboard/tech/change-admin-panel-style-description',
                'text'         => 'Change style',
                'icon'         => 'far fa-circle',
                'active_rules' => [
                    'urls' => [
                        'dashboard/tech/change-admin-panel-style-description'
                    ],
                ],
            ],
            [
                'text'     => 'Pages',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/tiles-list-page-description',
                        'text'         => 'Tiles list page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tiles-list-page',
                                'dashboard/tech/tiles-list-page-description',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/form-page',
                        'text'         => 'Form Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/form-page',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/login-page',
                        'text'         => 'Login Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/login-page',
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'Table',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/tables/manual-sorting',
                        'text'         => 'Manual sorting',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tables/manual-sorting',
                                'dashboard/tech/tables/manual-sorting-example',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/table-page-description',
                        'text'         => 'Table Page',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/table-page',
                                'dashboard/tech/table-page-description',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/tables/pagination',
                        'text'         => 'Pagination',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/tables/pagination',
                                'dashboard/tech/tables/pagination/base-using',
                                'dashboard/tech/tables/pagination/base-using-with-dropdown',
                                'dashboard/tech/tables/pagination/with-custom-per-page',
                                'dashboard/tech/tables/pagination/with-two-tables',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'Elements',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/main-menu',
                        'text'         => 'Main menu',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/main-menu',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/photo-uploading',
                        'text'         => 'Photo uploading',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/photo-uploading',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-dropdown',
                        'text'         => 'Date dropdown',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-dropdown',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/images',
                        'text'         => 'Images',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/images',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/visual-editor',
                        'text'         => 'Visual Editor',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/visual-editor',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/breadcrumbs',
                        'text'         => 'Breadcrumbs',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/breadcrumbs',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/notifications-description',
                        'text'         => 'Notifications',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => [
                                'dashboard/tech/notifications-description',
                                'dashboard/tech/notifications',
                            ],
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-range',
                        'text'         => 'Date range',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-range',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/date-range-js',
                        'text'         => 'Date range js',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/date-range-js',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/box',
                        'text'         => 'Box',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/box',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/flexible-date-time-picker',
                        'text'         => 'Flexible DateTime picker',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/flexible-date-time-picker',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/multifields',
                        'text'         => 'Multifields',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/multifields',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/lists',
                        'text'         => 'Lists',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/lists',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/image',
                        'text'         => 'Image',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/image',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/badge',
                        'text'         => 'Badge',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/badge',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/wrappers',
                        'text'         => 'Wrappers',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/wrappers',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/tabs',
                        'text'         => 'Tabs',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/tabs',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/grid',
                        'text'         => 'Grid',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/grid',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/links',
                        'text'         => 'Links',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/links',
                        ],
                    ],
                ],
            ],
            [
                'text'     => 'JS actions',
                'icon'     => 'fa-circle',
                'subitems' =>
                    [
                        [
                            'text'         => 'Fast JS Actions',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions',
                            ],
                        ],
                        [
                            'text'         => 'Mask',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/mask-description',
                            'active_rules' => [
                                'urls' => [
                                    'dashboard/tech/mask',
                                    'dashboard/tech/mask-description',
                                ],
                            ],
                        ],
                        [
                            'text'         => 'Confirmation popup',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/confirmation-popup',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/confirmation-popup',
                            ],
                        ],
                        [
                            'text'         => 'Tooltips',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/tooltips',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/tooltips',
                            ],
                        ],
                        [
                            'text'         => 'Content copy',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/content-copy-to-clipboard',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/content-copy-to-clipboard',
                            ],
                        ],
                        [
                            'text'         => 'Hide/Show block',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/hide-show-on-click',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/hide-show-on-click',
                            ],
                        ],
                        [
                            'text'         => 'Controlled Checkboxes',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/controlled-checkboxes',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/controlled-checkboxes',
                            ],
                        ],
                        [
                            'text'         => 'Delete with confirmation',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/delete-with-confirmation',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/delete-with-confirmation',
                            ],
                        ],
                        [
                            'link'         => 'dashboard/tech/js-actions/auto-update',
                            'text'         => 'Auto update elements',
                            'icon'         => 'far fa-circle',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/auto-update',
                            ],
                        ],
                        [
                            'text'         => 'Activity Controller',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/activity-controller',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/activity-controller',
                            ],
                        ],
                        [
                            'text'         => 'Open in modal',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/open-in-modal',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/open-in-modal',
                            ],
                        ],
                        [
                            'text'         => 'Send request',
                            'icon'         => 'far fa-circle',
                            'link'         => 'dashboard/tech/js-actions/send-request',
                            'active_rules' => [
                                'urls' => 'dashboard/tech/js-actions/send-request',
                            ],
                        ],
                    ],

            ],
            [
                'text'     => 'JS scripts',
                'icon'     => 'fa-circle',
                'subitems' => [
                    [
                        'link'         => 'dashboard/tech/js-scripts/sortable',
                        'text'         => 'Sortable',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/sortable',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/js-scripts/controlled-checkboxes',
                        'text'         => 'Controlled Checkboxes',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/controlled-checkboxes',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/js-scripts/date-range-picker',
                        'text'         => 'DateRangePicker',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/date-range-picker',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/js-scripts/notifications',
                        'text'         => 'Notifications',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/notifications',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/js-scripts/send-ajax',
                        'text'         => 'Sending Ajax',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/send-ajax',
                        ],
                    ],
                    [
                        'link'         => 'dashboard/tech/js-scripts/input-mask',
                        'text'         => 'Input mask',
                        'icon'         => 'far fa-circle',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/input-mask',
                        ],
                    ],
                    [
                        'text'         => 'File uploader',
                        'icon'         => 'far fa-circle',
                        'link'         => 'dashboard/tech/js-scripts/file-uploader',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/file-uploader',
                        ],
                    ],
                    [
                        'text'         => 'Dynamic MultiFields Simple',
                        'icon'         => 'far fa-circle',
                        'link'         => 'dashboard/tech/js-scripts/dynamic-multifields-simple',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/dynamic-multifields-simple',
                        ],
                    ],
                    [
                        'text'         => 'Dynamic MultiFields Complex',
                        'icon'         => 'far fa-circle',
                        'link'         => 'dashboard/tech/js-scripts/dynamic-multifields-complex',
                        'active_rules' => [
                            'urls' => 'dashboard/tech/js-scripts/dynamic-multifields-complex',
                        ],
                    ],
                ]
            ]
        ],
    ],
];
