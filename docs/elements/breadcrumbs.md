# BreadCrumbs

Basic breadcrumbs are generating automatically based on `MainMenu` component during in `DashboardServiceProvider::prepareBreadcrumbs()` function

## Adding additional elements to Breadcrumbs

Class `\Scigeniq\Dashboard\Elements\Breadcrumbs\Breadcrumbs` defined as a singleton. You can get it and modify it anywhere in the app.

For example, let's add additional active element to the end

```php
     app(Breadcrumbs::class)->add(          
            $link = '/',
            $text = 'index',
            $icon = 'fa-tachometer-alt'
        );
```

You can add as many elements as you need
Also, you can update all breadcrumbs by using function `\Scigeniq\Dashboard\Elements\Breadcrumbs\Breadcrumbs::updateItems`
For doing this you need to prepare `\Scigeniq\Dashboard\Elements\Breadcrumbs\BreadcrumbsItem` for every element.

```php
        $item1 = new \Scigeniq\Dashboard\Elements\Breadcrumbs\BreadcrumbsItem('/', 'Index', 'fa-tachometer-alt');
        $item2 = new \Scigeniq\Dashboard\Elements\Breadcrumbs\BreadcrumbsItem('/', 'Second element', 'fa-tachometer-alt');
        
         app(Breadcrumbs::class)->updateItems($item1, $item2);
```

You can also clear all breadcrumbs items by using `clear()` function

```php
         app(Breadcrumbs::class)->clear();
```