Functionality allows you to display image by IMG tag  
  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Image` class  

```php
$src = asset('/scigeniq/dashboard/img/default_logo.png');
$imageElement = new Image();
$imageElement->src($src);
$imageElement->alt('Scigeniq logo');
``` 

For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::image`