> The functionality allows you to create lists based on an array (key / value)

### DataList
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Links\Link` class  
```php
$preparedData = [
    'First name' => 'John',
    'Last name' => 'Doe',
    'Age' => '20',
];

$dataList = new DataList();
$dataList->data($preparedData);
```

### DescriptionList
Description List is another variant of the list  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Links\DescriptionList` class  
```php
$dataList = new DescriptionList();
$dataList->data($preparedData);
```
Also, you can make it 'horizontal' view
```php
$dataList->isHorizontal(true);
```
  
For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::lists`