> The functionality allows you to split content into different tabs without having to reload the page when switching between tabs.
> Let's take a closer look at them.
  
### Tabs
Container for tabs. All tabs are added to it  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Tabs\Tabs` class  

In order to display tabs, you need a container to store them.    
```php
$tabs = new Tabs();
```
After that, you can add tabs as a separate class instance.  
```php
$tab = new Tab('tab content');
```
or use **addTab** method built into **Tabs**, which returns a new **Tab** instance that can be immediately filled with content  
```php
$tabs->addTab()->content('tab content');
```
It is possible to use the **parent** method to add multiple tabs in a chain
```php
$tabs
    ->addTab()->content('tab1 content')->title('tab1 title')->active()
    ->parent()->addTab()->content('tab2 content')->title('tab2 title');
```

### Tab
It contains the contents of the tab, which the user sees if the tab is active    
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Tabs\Tab` class   
Adding Tab Content    
```php
$tab = new Tab();
$tab->content('tab content');
```
To Tab instance you can set title  
```php
$tab = new Tab();
$tab->title('tab title');
```
And also make it active, if necessary  
```php
$tab = new Tab();
$tab->active($default = true);
```
  
### Navigation
Pane with a list of tabs  
The functional is implemented using the `\Scigeniq\Dashboard\Elements\Tabs\Navigation` class  
If you need to add attributes or set a class for the tab Navigation bar, then it can be obtained through the **Tabs** instance  
```php
$navigation = $tabs->getNavigation();
```

If the Navigation is implemented before Tabs, then it can be passed inside the Tabs instance  
```php
$navigation = new Navigation();
/* ... */
$tabs->navigation($navigation);
```

For a more detailed example, see `\Scigeniq\Dashboard\Docs\Http\PresentationController::tabs`