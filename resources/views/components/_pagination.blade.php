@if ($paginated_items->hasPages())
    <div class="row align-items-baseline">

        <div class="col-sm-12 col-md-6">
            <div class="clearfix py-3 d-flex align-items-baseline justify-content-start">
                <div class="pagination-txt">{{ __("common.pagination.showing") }} {{$paginated_items->firstItem()}} {{ __("common.pagination.to") }} {{$paginated_items->lastItem()}} {{ __("common.pagination.of") }} {{$paginated_items->total()}} {{ __("common.pagination.entries") }}</div>
                @if($steps_change)
                        <div class="form-group row ml-4">
                            <label for="entries-per-page" class="col-form-label pr-2">Entries per page</label>
                            <select id="entries-per-page"
                                    name="{{$pagination_step_param}}"
                                    class="form-control w-auto js_ajax-by-change"
                                    data-method="GET"
                                    data-action="{{$step_update_action ?: $action}}"
                                    data-replace-blk="{{$result_block_class}}">
                                @foreach($available_steps as $step)
                                    <option @if($step == $per_page) selected @endif value="{{$step}}">{{$step}}</option>
                                @endforeach
                            </select>
                        </div>
                @endif
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="clearfix py-3">
                <ul class="pagination m-0" {!! $dynamic_fields !!}>
                    <li class="page-item previous">
                        <a href="#" class="page-link js_btn-pagination @if($paginated_items->currentPage() == 1)btn disabled @endif"
                           data-action="{{$action}}"
                           data-method="GET"
                           data-page="{{$paginated_items->currentPage() - 1}}"
                           data-replace-blk="{{$result_block_class}}">{{ __("common.pagination.previous") }}</a>
                    </li>
                    @for($i=1; $i <= $paginated_items->lastPage(); $i++)
                        @if ($i === $paginated_items->currentPage())
                            <li class="page-item active">
                                <a href="#"
                                   class="page-link js_btn-pagination"
                                   data-page="{{$i}}"
                                   data-method="GET"
                                   data-action="{{$action}}"
                                   data-replace-blk="{{$result_block_class}}">{{ $i }}</a>
                            </li>
                        @elseif(abs($paginated_items->currentPage() - $i) < 2 || $i <= 2 || ($paginated_items->lastpage() - $i) < 2)
                            <li class="page-item">
                                <a href="#"
                                   class="page-link js_btn-pagination"
                                   data-page="{{$i}}"
                                   data-method="GET"
                                   data-action="{{$action}}"
                                   data-replace-blk="{{$result_block_class}}">{{ $i }}</a>
                            </li>
                        @elseif(abs($paginated_items->currentPage() - $i) === 3)
                            <li class="page-item">
                                <a href="#"
                                   class="page-link js_btn-pagination disabled"
                                   data-action="{{$action}}"
                                   data-page="{{$i}}"
                                   data-method="GET"
                                   data-replace-blk="{{$result_block_class}}">...</a>
                            </li>
                        @endif
                    @endfor
                    <li class="page-item next">
                        <a href="#" class="page-link js_btn-pagination @if($paginated_items->currentPage() == $paginated_items->lastPage())btn disabled @endif"
                           data-action="{{$action}}"
                           data-method="GET"
                           data-page="{{$paginated_items->currentPage() + 1}}"
                           data-replace-blk="{{$result_block_class}}">{{ __("common.pagination.next") }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endif
