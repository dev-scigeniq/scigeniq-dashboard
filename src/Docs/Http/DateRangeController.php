<?php


namespace Scigeniq\Dashboard\Docs\Http;



use Carbon\Carbon;
use Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Dashboard;
use Scigeniq\Dashboard\Elements\Forms\Elements\FormGroup;
use Scigeniq\Dashboard\Elements\Links\LinkButton;

class DateRangeController
{
    /**
     * Tooltips functionality presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function example(Dashboard $dashboard) : Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/date-range.md');

        $dateRangeExample = (new FormGroup())->element()->dateRange()->name('selectName1');
        $groupDateRangeDefaultExample = (new FormGroup())->labelTxt('Default DateRange');
        $groupDateRangeDefaultExample->labelId($dateRangeExample->id)->formGroupContent($dateRangeExample);


        $dateRangeCustomExample = (new FormGroup())
            ->element()
            ->dateRange()
            ->clearRanges()
            ->addRange(
                'Last 1 days (except today)',
                Carbon::now()->subDays(1),
                Carbon::yesterday()
            )
            ->addCurrentYear('Current year with my title')
            ->addEmptyLine('! Empty value !')
            ->addRange(
                'Last 5 days (except today)',
                Carbon::now()->subDays(5),
                Carbon::yesterday()
            )
            ->addRange(
                'My Today',
                Carbon::today()
            )
            ->addPreviousMonth()
            ->setDateFormat('Y-d-m')
            ->name('selectName2')
            ;
        $groupDateRangeCustomExample = (new FormGroup())->labelTxt('Custom DateRange');
        $groupDateRangeCustomExample->labelId($dateRangeCustomExample->id)->formGroupContent($dateRangeCustomExample);

        $dateRangeExample2 = (new FormGroup())
            ->select('select', [3 => 'Option 3', 4 => 'Option 4'], 4, 'Select me2', false);


        $dashboard->page()
            ->setPageTitle('')
            ->addElement()->tabs()->addTab()->title('Description')->content($content)->active()
            ->parent()->addTab()->title('Example')->content([
                $groupDateRangeDefaultExample,
                '<br>',
                $groupDateRangeCustomExample,
                $dateRangeExample2
            ]);

        return $dashboard;
    }
}
