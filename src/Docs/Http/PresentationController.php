<?php


namespace Scigeniq\Dashboard\Docs\Http;

use Carbon\Carbon;
use Exception;
use Faker\Generator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Storage;
use Scigeniq\Dashboard\Components\FormGenerator;
use Scigeniq\Dashboard\Components\FormPageGenerator;
use Scigeniq\Dashboard\Components\TableGenerator;
use Scigeniq\Dashboard\Components\TablePageGenerator;
use Scigeniq\Dashboard\Components\TilesListPageGenerator;
use Scigeniq\Dashboard\Core\Content\ContentFieldsUsable;
use Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Dashboard;
use Scigeniq\Dashboard\Elements\Badge;
use Scigeniq\Dashboard\Elements\Forms\Elements\FlexibleDateTimePicker;
use Scigeniq\Dashboard\Elements\Forms\Elements\FormGroup;
use Scigeniq\Dashboard\Elements\Grid\Grid;
use Scigeniq\Dashboard\Elements\Icons\Icon;
use Scigeniq\Dashboard\Elements\Lists\DataList;
use Scigeniq\Dashboard\Elements\Lists\DescriptionList;
use Scigeniq\Dashboard\Elements\Image;
use Scigeniq\Dashboard\Elements\StringElement;
use Scigeniq\Dashboard\Elements\Links\LinkButton;
use Scigeniq\Dashboard\Elements\Links\LinkJSDelete;
use Scigeniq\Dashboard\Elements\Tables\TableCell;
use Scigeniq\Dashboard\Elements\Tables\TableTitle;
use Scigeniq\Dashboard\Elements\Titles\H4Title;
use Scigeniq\Dashboard\Elements\Tabs\Navigation;
use Scigeniq\Dashboard\Elements\Tabs\Tab;
use Scigeniq\Dashboard\Elements\Tabs\Tabs;
use Scigeniq\Dashboard\Elements\WrapperDiv;
use Scigeniq\Dashboard\Elements\WrapperSpan;
use Scigeniq\Dashboard\JsActions\Mask;
use Scigeniq\Dashboard\Elements\Boxes\Box;
use Scigeniq\Dashboard\Elements\Breadcrumbs\Breadcrumbs;
use Scigeniq\Dashboard\Elements\Buttons\DefaultButton;
use Scigeniq\Dashboard\Elements\Factories\ElementsCreateAbleContract;
use Scigeniq\Dashboard\Elements\Factories\ElementsFactory;
use Scigeniq\Dashboard\Elements\Forms\Elements\Input;
use Scigeniq\Dashboard\Elements\Links\Link;
use Scigeniq\Dashboard\Pages\LoginPage;
use Scigeniq\Dashboard\Services\FakeDataService;

class PresentationController
{
    /**
     * Table page generation description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function installation(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/installation.md');

        $dashboard->page()
            ->addElement()
            ->box()->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * Table page generation description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function tablePageDescription(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/pages/table-page.md');

        $dashboard->page()
            ->addElement()
            ->box()
            ->addToolsLinkButton(
                route('dashboard.docs.presentation.table-page'),
                'Example',
                '',
                'btn-info'
            )
            ->content($content);

        return $dashboard;
    }

    /**
     * Prepare Table Page for presentation
     *
     * @return mixed
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function tablePage()
    {
        /** @var Generator $faker */
        $faker = app(Generator::class);
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'id'      => $faker->numberBetween(0, 100),
                'name'    => $faker->name,
                'address' => $faker->address,
            ];
        }

        $paginator = new LengthAwarePaginator($data, 100, 10, 5);

        $tableGenerator = (new TablePageGenerator())
            ->title('Page title', 'Page subtitle')
            // Add items
            ->items($data)
            // Set titles
            ->tableTitles('ID', 'Address')
            // Alternative setting titles
            ->tableTitles(['ID', 'Address'])
            // Add additional title
            ->addTableTitle('Address && Name')
            ->addTableTitle((new TableTitle('Table title'))->attr('some-attr', 'some-value'))
            ->addTableTitle('New field static')
            // Add title with sorting
            ->addTitleWithSorting('New field', 'sortingFieldName', 'desc', true, request()->url(), 'GET')
            // Limit fields to show
            ->showOnly('id', 'address', 'address-name', 'new-field', 'new-field2', 'second-new-field')
            // Add additional fields and items  fields handlers
            ->setConfig([
                'address'      => function ($item) {
                    return (new Link())->content($item['address'])->link('/');
                },
                'address-name' => function ($item) {
                    return '<b>' . $item['name'] . '</b> (' . $item['address'] . ')';
                },
                'new-field'    => 'New field static content',
                'new-field2'    => (new TableCell('New field static content2'))->attr('custom-attr', 'custom-value'),
            ])
            // Add one additional field to content
            ->addToConfig('second-new-field', function ($item) {
                return 'New field static - ' . $item['id'];
            })
            // Add crude links
            ->createLink(url('/'))
            ->setEditLinkClosure(function ($item) {
                return url('/edit', ['id' => $item['id']]);
            })
            ->setShowLinkClosure(function ($item) {
                return url('/view', ['id' => $item['id']]);
            })
            ->setDestroyLinkClosure(function ($item) {
                return url('/destroy', ['id' => $item['id']]);
            })
            // Add custom element to tool column
            ->addElementsToToolsCollection(function ($item) {
                return (new DefaultButton())
                    ->content($item['name'])->js()->tooltip()->regular('Test button description');
            })
            // Add pagination
            ->withPagination($paginator, request()->url())
            // Add additional tool button
            ->addToolsLinkButton('/', 'New tool button', 'fas fa-plus')
            // Activate bulk actions functionality
            ->bulkActions([
                url()->current() => 'Action 1',
                url('/')         => 'Action 2',
            ], function ($item) {
                return $item['id'];
            })
            // Manual sorting activation
            ->manualSorting(url()->current(), function ($item) {
                return $item['id'];
            }, 'GET');

        $tableGenerator->getPage()->showDangerNotification('Danger!', 'Lorem ipsum...');

        // Add filtering
        $tableGenerator->addFiltering()
            ->action(request()->url())
            ->method('GET')
            ->simpleSelect('name', ['Dan', 'Vincent'], request(), 'Name', true)
            ->dateTimeInput('date', today(), 'Date', false)
            ->submitButton('Filter')
            ->clearButton('Clear');

        return $tableGenerator;
    }

    /**
     * Login page generation description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function loginPageDescription(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/pages/login-page.md');

        $dashboard->page()
            ->addElement()
            ->box()
            ->makeSimple()
            ->addToolsLinkButton(
                route('dashboard.docs.presentation.login-page-demo'),
                'Example',
                '',
                'btn-info'
            )
            ->content($content);

        return $dashboard;
    }

    /**
     * Login page generation demo
     *
     * @param LoginPage $page
     *
     * @return LoginPage
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function loginPageDemo(LoginPage $page): LoginPage
    {
        $page->setDefaultForm();

        return $page;
    }

    /**
     * Show tiles list page description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function tilesListPageDescription(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/pages/tiles-list-page.md');

        $dashboard->page()
            ->addElement()
            ->box()
            ->addToolsLinkButton(
                route('dashboard.docs.presentation.tiles-list-page'),
                'Example',
                '',
                'btn-info'
            )
            ->content($content);

        return $dashboard;
    }

    /**
     * Show tiles list page demosntration
     *
     * @return string|TilesListPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function tilesListPage()
    {
        /** @var Generator $faker */
        $faker = app(Generator::class);
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'id'      => $faker->numberBetween(0, 100),
                'name'    => $faker->name,
                'address' => $faker->address,
            ];
        }

        $paginator = new LengthAwarePaginator($data, 100, 10, 5);

        $tilesListPage = (new TilesListPageGenerator())
            ->title('Tiles page title', 'Tiles page subtitle')
            // Add items
            ->setItems($data)
            // Limit fields to show
            ->setOnly('name', 'tmp', 'second-new-field')
            ->setConfig([
                'tmp' => function (array $item) {
                    return $item['name'] . ' : ' . $item['address'];
                },
            ])
            // Add one additional field to content
            ->addToConfig('second-new-field', function ($item) {
                return 'New field startic - ' . $item['id'];
            })
            // Set closure to replace the default rendering
            ->setItemRenderingClosure(function ($item) {
                return app(ElementsFactory::class)
                    ->box()->content($item['name'])->makeSimple();
            })
            // Add pagination
            ->withPagination($paginator, url()->current());

        $tilesListPage->getPage()->showInfoNotification('Info!', 'Lorem ipsum...');

        // Add filtering
        $tilesListPage
            // Filter into the box
            ->addFiltering(true)
            ->simpleSelect('name', ['Dan', 'Vincent'], request(), 'Name', true)
            ->dateTimeInput('date', today(), 'Date', false)
            ->submitButton('Filter');

        if (request()->ajax()) {
            return $tilesListPage->prepareContent();
        }

        return $tilesListPage;
    }

    /**
     * Prepare Form Page for presentation
     *
     * @throws Exception
     */
    public function formPage()
    {
        $dashboard = new Dashboard();
        $content = view()->file(__DIR__ . '/../../../docs/pages/form-page-tutor.md');

        $img = 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';

        /** @var FormPageGenerator $formPageGenerator */
        $formPageGenerator = (new FormPageGenerator())
            ->title('Page title', 'Page sub-title')
            ->method('POST')    // default POST
            ->action('/')        // default '/'
            ->ajax(true)         // set form to send with Ajax. Default 'true'
            ->input('test_name', 'Submit with custom input', '', 'submit', false, '', [], 'btn btn-default')
            ->hiddenInput('hidden_attribute', null)
            ->textInput('name', null, 'Name', true)
            ->slugInput('slug', 'name', null, 'Slug generated automatically based on name', false, '-', 'lowercase')
            ->numberInput('number', 0, 'Number input', false, 0.01)
            ->emailInput('email', 'tesdt@email.com', 'Email', true)
            ->passwordInput('password', '123', 'Password', true)
            ->colorInput('color', '#000000', 'Select color', false)
            ->checkbox('checkbox_name', false, 'Check me')
            ->switcher('switcher_name', true, 'Switch me')
            // Regular date input
            ->dateInput('date', today(), 'Date', true)
            // Date picker JS
            ->datePickerJS('date_js', today()->format('d-m-Y'), 'Select date with JS', true, ['format' => 'DD-MM-YYYY'], true, true, 2015, 2025, true, false)
            // Date range picker
            ->dateRangePicker('date_range_start', 'date_range_end', today(), today(), 'Select range of dates', true, true, [],true, true, 2015, 2025, true, false)
            // Regular time picker
            ->timeInput('time', now(), 'Time')
            // Time picker JS
            ->timePickerJS('time_js', now(), 'Select time with JS', false, [], true, true, true, 2015, 2025, false)
            // Regular date and time input
            ->dateTimeInput('date_time', now(), 'DateTime', false)
            // Date and time picker with JS
            ->dateTimePickerJS('date_time_js', now(), 'Select date and time with JS (12h-format without seconds)', false, [], false, false, true, true, 2015, 2025, false)
            // Date and time range picker
            ->dateTimeRangePicker('date_time_range_start', 'date_time_range_end', today(), today(),
                'Select range of dates and times', false, false, [], true, true, true,
                false, 2015, 2025, false)
            // Date range
            ->dateRange('date_range', 'Date range')
            // Regular select
            ->select('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me', false)
            // Multiply select
            ->select('select', [1 => 'Option 1', 2 => 'Option 2', 3 => 'Option 3'], [1,3], 'Select me twice', false, true)
            // Regular JS select
            ->selectJS('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me with JS', false)
            // JS Select with autocomplete on back-end
            ->selectWithAutocomplete('select', route('dashboard.docs.presentation.select-autocomplete'), [1 => 'London', 5 => 'Paris'], 1, 'Search with back-end autocomplete', false, true)
            ->textarea('comment', '', 'Comment')
            ->visualEditor('content', '<p>test</p>', 'Editor', true) // Additional params can turn on image uploading functionality
            ->fileInput('file', request(), 'File')
            ->imageInput('testImag', $img, 'Image block', '20 Mb', '10', '234', $img, 'myImage.png')
            ->submitButtonTitle('Push me')
            // Add additional button to submit the form with additional params which will be send to backend
            ->addSubmitButton(['redirect' => url('dashboard')], 'Submit and back to dashboard')
            // Add additional link button
            ->addLinkButton(url('/'), 'Go home')
            ->clearButton();

        $formPageGenerator->getForm()->sendAllCheckbox(true);

        $formPageGeneratorWitFormGroup = (new FormPageGenerator())
            ->title('Creating new movement from account to account ')
            ->action('/');

        $formPageGeneratorWitFormGroup->getBox()->element()->grid([
            (new Box())
                ->showFullscreenBtn()
                ->boxHeaderContent('<b>From</b>')
                ->content([
                    (new FormGroup())->select('outgoing_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
                    (new FormGroup())->dateInput('outgoing_date', now(), 'Date', true),
                    (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
                    (new FormGroup())->numberInput('outgoing_commission', 0, 'Commission', false, 0.01),
                    (new FormGroup())->select('outgoing_contractor_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Contractor'),
                ]),
            (new Box())
                ->boxHeaderContent('&nbsp')
                ->content([
                    (new FormGroup())->numberInput('rate', 1, 'Rate', true, 0.01),
                    (new FormGroup())->textarea('comment', '', 'Comment', false, ['rows' => 8]),
                ]),
            (new Box())
                ->boxHeaderContent('<b>To</b>')
                ->content([
                    (new FormGroup())->select('incoming_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
                    (new FormGroup())->datePickerJS('incoming_date', now(), 'Date', true),
                    (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
                    (new FormGroup())->numberInput('incoming_commission', 0, 'Commission', false, 0.01),
                    (new FormGroup())->select('incoming_contractor_id', array_prepend([1 => 'Option 1', 2 => 'Option 2'], '', ''), '', 'Contractor'),
                ]),
        ])->lgRowCount(3);

        $formPageGeneratorWitFormGroup->submitButtonTitle('Make transaction and go to it');

        $dashboard->page()
            ->setPageTitle('Form Page')
            ->addElement()->tabs()->addElement()->tab()->title('Description')->content($content)->active()
            ->parent()->addElement()->tab()->title('Example (FormPageGenerator with FormGroup)')->content($formPageGeneratorWitFormGroup->getBox()->render())
            ->parent()->addElement()->tab()->title('Example (FormPageGenerator)')->content($formPageGenerator->getBox()->render())
        ;
        return $dashboard;
    }

    /**
     * Date Dropdown element presentation
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     */
    public function dateDropdown(Dashboard $dashboard)
    {
        $dashboard->page()->element()->box()->headerAvailable(false)->footerAvailable(false)
            ->element()->form()
            ->element()->dateDropdown()
            ->options([
                Carbon::today()->subWeek() . '/' . Carbon::today()   => 'Last week',
                Carbon::today()->subMonth() . '/' . Carbon::today()  => 'Last month',
                Carbon::today()->subMonth(3) . '/' . Carbon::today() => 'Last 3 months',
                Carbon::today()->subYear() . '/' . Carbon::today()   => 'Last year',
            ]);

        return $dashboard;
    }

    /**
     * Page for demonstration image displaying components
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function imageDisplaying(Dashboard $dashboard)
    {
        $elFactory = app(ElementsFactory::class);

        $img = 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';

        // Simple image preview
        $el = $elFactory->imagePreview();

        $el2 = $elFactory->imagePreview()
            ->imgUrl($img)
            ->size('100 Mb')
            ->downloadUrl($img);

        // Image input for using inside form
        $el3 = $elFactory->imageInput()
            ->addClass('col-6  col-md-4 col-lg-3')
            ->imgUrl($img)
            ->size('10 Mb')->width('50')->height('14')
            ->title('Cool image component');

        // Stand alone full functional iamge input
        $el4 = $elFactory->imageComponent()
            ->addClass('col-6  col-md-4 col-lg-3')
            ->imgUrl($img)
            ->size('10 Mb')->width('50')->height('14')
            ->title('Cool image component');

        $dashboard->addContent(['<div class="row">', $el3, $el4, '</div>', $el, $el2]);

        return $dashboard;
    }

    /**
     * JS actions description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function jsActions(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/js-actions.md');

        $dashboard->page()->addElement()
            ->box()
            ->headerAvailable(false)
            ->content($content);

        return $dashboard;
    }

    /**
     * @return JsonResponse
     */
    public function autoComplete()
    {
        $data = [
            'results' => [
                [
                    'id' => 1,
                    'text' => 'London',
                ],
                [
                    'id' => 2,
                    'text' => 'New York',
                ]
            ],
        ];

        return response()->json($data);
    }

    /**
     * Content auto update demonstration
     *
     * @param Dashboard $dashboard
     *
     * @return mixed|ContentFieldsUsable|Dashboard|ElementsCreateAbleContract
     * @throws NoOneFieldsWereDefined
     */
    public function autoUpdate(Dashboard $dashboard)
    {
        $docs = view()->file(__DIR__ . '/../../../docs/js-actions/auto-update.md');

        $button = $this->autoUpdateButton();

        $dashboard->page()
            ->setPageTitle('Auto update element page')
            ->addElement()->tabs()->addElement()->tab()->title('Description')->content($docs)->active()
            ->parent()->addElement()->tab()->title('Example')->content($button->render());

        return $dashboard;
    }

    /**
     * @return DefaultButton
     * @throws NoOneFieldsWereDefined
     */
    public function autoUpdateButton(): DefaultButton
    {
        $icon = new Icon('fa-cog fa-spin');
        $serverTimeStringFormatted = 'Updating button < Server time - ' . now()->format('H:i:s') . ' >';
        $button = new DefaultButton();
        $button->content($icon . $serverTimeStringFormatted);
        $button->js()->contentAutoUpdate()->replaceCurrentElWithContent(route('dashboard.docs.presentation.js-actions.auto-update-button'),
            'GET', 5000);

        return $button;
    }

    public function maskDescription(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__.'/../../../docs/elements/mask-element.md');

        $dashboard->page()
            ->addElement()
            ->box()
            ->addToolsLinkButton(route('dashboard.docs.presentation.mask'),
                'Example',
                '',
                'btn-info'
            )
            ->content($content);

        return $dashboard;
    }



    /**
     * Notifications generation description
     *
     * @param Dashboard $dashboard
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function notificationsDescription(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/notifications.md');
        $dashboard->page()
            ->addElement()
            ->box()
            ->addToolsLinkButton(route('dashboard.docs.presentation.notifications'),
                'Example',
                '',
                'btn-info'
            )
            ->content($content);

        return $dashboard;
    }



    public function mask(Dashboard $dashboard): Dashboard
    {
        /*
                в чём разница?


                $element = new Input();
                $element->js()->mask()->number();
                $dashboard->addContent($element);

                и
                Dashboard $dashboard
                $dashboard->addContent((new Mask(new Input()))->number())
                return $dashboard;
        */
        $dashboard->addContent($this->makeMaskElement('number', 'Number'));
        $dashboard->addContent($this->makeMaskElement('email', 'Email'));
        $dashboard->addContent($this->makeMaskElement('number2DecPlaces', 'Number with two decimal places'));
        $dashboard->addContent($this->makeMaskElement('dateEuro24', 'European date and time 24h format'));
        $dashboard->addContent($this->makeMaskElement('dateUSA12', 'Date USA and time 12h format'));
        $dashboard->addContent($this->makeMaskElement('time12h', 'Time 12h format'));
        $dashboard->addContent($this->makeMaskElement('time24h', 'Time 24h format'));
        $dashboard->addContent($this->makeMaskElement('dateUSA', 'USA date format'));
        $dashboard->addContent($this->makeMaskElement('dateEuro', 'European date format'));

        return $dashboard;
    }

    protected function makeMaskElement($maskName, $labelTxt = 'label'): FormGroup
    {
        $element = (new Mask(new Input()))->$maskName();
        $groupElement = (new FormGroup())->labelTxt($labelTxt);
        $groupElement->labelId($element->id)->formGroupContent($element);
        return $groupElement;
    }

    /**
     * Notification examples
     *
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function notifications(Dashboard $dashboard)
    {
        $dashboard->page()
            ->showSuccessNotification('Success!', 'Lorem ipsum')
            ->showInfoNotification('Info!', 'Lorem ipsum', false)
            ->showWarningNotification('Warning!', 'Lorem ipsum', false, true)
            ->showDangerNotification('Danger!', 'Lorem ipsum', false, true, 2);

        return $dashboard;
    }

    /**
     * Files uploading example
     *
     * @param Request $request
     *
     * @return FormPageGenerator|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function photoUploading(Request $request)
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/photo-uploads.md');
        $box = (new Box())->makeSimple()->content($content);

        $loadedPhotos = [
            'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg',
            config('scigeniq.dashboard.dashboard.default_image')
        ];

        $formPageGenerator = (new FormPageGenerator())->imageArea('photos[]', $loadedPhotos, 'Photo upload');

        $formPageGenerator->getPage()->addContent($box, 'data', true);

        return $formPageGenerator;
    }

    /**
     * @param Dashboard $dashboard
     *
     * @return Dashboard|Breadcrumbs
     * @throws NoOneFieldsWereDefined
     */
    public function breadcrumbs(Dashboard $dashboard)
    {
        app(Breadcrumbs::class)->clear();

        $content = view()->file(__DIR__ . '/../../../docs/elements/breadcrumbs.md');
        $dashboard->page()
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function mainMenu(Dashboard $dashboard)
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/main-menu-element.md');
        $dashboard->page()
            ->addElement()
            ->box()
            ->makeSimple()
            ->content($content);

        return $dashboard;
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function adminPanelStyleDescription(Dashboard $dashboard): Dashboard
    {
        $content = view()->file(__DIR__ . '/../../../docs/core/admin-panel-style.md');

        $dashboard->page()->addElement()->box()->makeSimple()->content($content);

        return $dashboard;
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function visualEditor(Dashboard $dashboard)
    {
        $description = (new Box())->makeSimple()->content(view()->file(__DIR__ . '/../../../docs/elements/visual-editor.md'));
        $exampleBox = (new Box())->makeSimple()->content(
            (new FormGenerator())->visualEditor('visual', '', 'Visual Editor Example')
        );

        $dashboard->page()->content($description, $exampleBox);

        return $dashboard;
    }

    /**
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function box(): Dashboard
    {
        $dashboard = new Dashboard();
        $content = view()->file(__DIR__ . '/../../../docs/elements/box.md');

        $items = (new FakeDataService())->forTable(3);

        $example = (new FormPageGenerator())->getBox()->content(
            (new Box())
                ->addBoxType()
                ->addBoxTitle('<b>This form can be expanded to full screen</b>')
                ->addBoxTools()
                ->addBoxBody([
                    'content1',
                    (new TableGenerator())->items($items),
                ])
                ->addBoxFooter('Footer')
                ->addClass(true)
                ->addBoxBodyClasses('')
                ->addBoxHeaderContent('')
                ->addHeaderAvailable(true)
                ->addFooterAvailable(true)
                ->addColorType()      // (default/primary/info/warning/success/danger)
                ->addSolid(true)
                ->showFullscreenBtn()
        );

        $dashboard->page()
            ->setPageTitle('')
            ->addElement()
            ->tabs()
            ->addTab()->title('Description')->content($content)->active()
            ->parent()
            ->addTab()->title('Example')->content($example);

        return $dashboard;
    }

    /**
     * Flexible DateTime Picker presentation
     *
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function flexibleDateTimePicker() : Dashboard
    {
        $dashboard = new Dashboard();

        $content = view()->file(__DIR__ . '/../../../docs/elements/flexible-date-time-picker.md');

        $flexibleDateTimePicker = (new FlexibleDateTimePicker())
            ->name('flexible_datetime_picker')
            ->value(now())
            ->horizontalPosition('right')
            ->verticalPosition('bottom')
            ->showWeekNumber(false)
            ->toolbarPosition('top')
            ->minYear(now()->subYears(5)->year)
            ->maxYear(now()->addYears(5)->year)
            ->viewMode('months');

        $flexibleDateTimePickerWithFormattedDate = (clone $flexibleDateTimePicker)->setDateFormat('Y-m');

        $dashboard->page()
            ->setPageTitle('Flexible Date Time Picker')
            ->addElement()->tabs()->addTab()->title('Description')->content($content)->active()
            ->parent()->addTab()->title('Example')->content(new WrapperDiv([
                $flexibleDateTimePicker,
                $flexibleDateTimePickerWithFormattedDate,
            ]));

        return $dashboard;
    }

    /**
     * @return \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareMultifieldsSimpleElement(): \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement
    {
        $multifieldsSimpleElement = new \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement();
        $multifieldsSimpleElement->maxCopyCount(5);
        $multifieldsSimpleElement->addFields(
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Input())->name('address'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea())->name('textarea'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\DateRange())->name('dateRange'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\DateTimePicker())->name('dateTimePicker'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS())->name('selectJS')->options([1,2,3]),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Select)->name('select')->options([1,2,3])
        );

        $multifieldsSimpleElement->setData(
            [
                [
                    'address' => 'address1',
                    'textarea' => 'textarea2',
                    'dateRange' => now(),
                    'dateTimePicker' => now()->addDay(),
                    'selectJS' => ['a' => 123, 'b', 'c'],
                    'select' => ['a' => 456, 'b' => 789, 'c'],
                ],
                [
                    'address' => 'address2',
                    'textarea' => 'textarea2',
                    'dateRange' => now()->addDays(5),
                    'dateTimePicker' => now()->addDays(6),
                    'selectJS' => ['c', 'd', 'e'],
                    'select' => ['a' => 101112, 'b' => 131415, 'c'],
                ],
            ]
        );

        return $multifieldsSimpleElement;
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareMultifieldsSimpleElementTab(): WrapperDiv
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/multifields-simple.md');

        $multifieldsSimpleElement = $this->prepareMultifieldsSimpleElement();

        $formGenerator = new \Scigeniq\Dashboard\Components\FormGenerator();
        $formGenerator->action('/dashboard/tech/multifields-test');
        $formGenerator->getForm()->addContent($multifieldsSimpleElement);
        $formGenerator->addSubmitButton([],'Submit', 'btn btn-primary float-left ml-2');

        $wrapper = new WrapperDiv();
        $wrapper->addContent((new Box($content))->headerAvailable(false)->footerAvailable(false));
        $wrapper->addContent((new Box($formGenerator))->headerAvailable(true)->footerAvailable(false)->boxHeaderContent('Example'));

        return $wrapper;
    }

    /**
     * @return \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareMultifieldsComplexElement(): \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsSimpleElement
    {
        $multifieldsComplexElement = new \Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsComplexElement('entity_given_name');
        $multifieldsComplexElement->maxCopyCount(5);
        $multifieldsComplexElement->setAddButtonUrl('/dashboard/tech/multifields-add', 'POST');
        $multifieldsComplexElement->setRemoveButtonUrl('/dashboard/tech/multifields-delete', 'DELETE');

        $multifieldsComplexElement->addFields(
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Input())->name('address'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea())->name('textarea'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\DateRange())->name('dateRange'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\DateTimePicker())->name('dateTimePicker'),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS())->name('selectJS')->options([1,2,3]),
            (new \Scigeniq\Dashboard\Elements\Forms\Elements\Select)->name('select')->options([1,2,3])
        );

        $multifieldsComplexElement->setData(
            [
                [
                    'id' => '12312321',
                    'address' => 'address1',
                    'textarea' => 'textarea2',
                    'dateRange' => now(),
                    'dateTimePicker' => now()->addDay(),
                    'selectJS' => ['a' => 123, 'b', 'c'],
                    'select' => ['a' => 456, 'b' => 789, 'c'],
                ],
                [
                    'id' => '978734534',
                    'address' => 'address2',
                    'textarea' => 'textarea2',
                    'dateRange' => now()->addDays(5),
                    'dateTimePicker' => now()->addDays(6),
                    'selectJS' => ['c', 'd', 'e'],
                    'select' => ['a' => 101112, 'b' => 131415, 'c'],
                ],
            ]
        );

        return $multifieldsComplexElement;
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareMultifieldsComplexElementTab(): WrapperDiv
    {
        $content = view()->file(__DIR__ . '/../../../docs/elements/multifields-complex.md');

        $multifieldsComplexElement = $this->prepareMultifieldsComplexElement();

        $formGenerator = new \Scigeniq\Dashboard\Components\FormGenerator();
        $formGenerator->action('/dashboard/tech/complex-multifields-test');
        $formGenerator->getForm()->addContent($multifieldsComplexElement);
        $formGenerator->submitButton('Submit');
        $formGenerator->addSubmitButton([],'Submit', 'btn btn-primary float-left ml-2');

        $wrapper = new WrapperDiv();
        $wrapper->addContent((new Box($content))->headerAvailable(false)->footerAvailable(false));
        $wrapper->addContent((new Box($formGenerator))->headerAvailable(true)->footerAvailable(false)->boxHeaderContent('Example'));

        return $wrapper;
    }

    /**
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function multifields(): Dashboard
    {
        $dashboard = new Dashboard();

        $dashboard->page()
            ->addElement()->tabs()->addTab()->title('Multifields simple')->content($this->prepareMultifieldsSimpleElementTab()->render())->active()
            ->parent()->addTab()->title('Multifields complex')->content($this->prepareMultifieldsComplexElementTab()->render());

        return $dashboard;
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function badge(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/badge.md');

        $example = $this->prepareBadgeExample();

        $dashboard->page()
            ->setPageTitle('Badge')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Example')->content($example);

        return $dashboard;
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareBadgeExample(): WrapperDiv
    {
        return new WrapperDiv([
            (new Badge('Badge'))->addClass('badge-primary'),
            (new Badge('Badge'))->addClass('badge-secondary'),
            (new Badge('Badge'))->addClass('badge-success'),
            (new Badge('Badge'))->addClass('badge-danger'),
            (new Badge('Badge'))->addClass('badge-warning'),
            (new Badge('Badge'))->addClass('badge-info'),
            (new Badge('Badge'))->addClass('badge-light'),
            (new Badge('Badge'))->addClass('badge-dark'),
        ]);
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function image(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/image.md');

        $example = $this->prepareImageExample();

        $dashboard->page()
            ->setPageTitle('Image')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Example')->content($example);

        return $dashboard;
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareImageExample(): WrapperDiv
    {
        return (new WrapperDiv(
            (new Image(asset('/scigeniq/dashboard/img/default_logo.png')))->alt('Scigeniq logo')
        ))->addClass('text-center');
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function lists(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/lists.md');

        $examples = new WrapperDiv([
            new H4Title('DataList'),
            $this->prepareDataList()->addClass('mt-4 mb-4'),
            new H4Title('DescriptionList'),
            $this->prepareDescriptionList()->addClass('mt-4 mb-4'),
            new H4Title('DescriptionList (horizontal)'),
            $this->prepareHorizontalDescriptionList()->addClass('mt-4 mb-4'),
        ]);

        $dashboard->page()
            ->setPageTitle('Lists')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)
            ->parent()->addTab()->title('Example')->content($examples->render())->active();

        return $dashboard;
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareDataList(): WrapperDiv
    {
        return new WrapperDiv(
            (new DataList([
                'First name' => 'John',
                'Last name' => 'Doe',
                'Age' => '20',
            ]))
        );
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareDescriptionList(): WrapperDiv
    {
        return new WrapperDiv(
            (new DescriptionList([
                'First name' => 'John',
                'Last name' => 'Doe',
                'Age' => '20',
            ]))
        );
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareHorizontalDescriptionList(): WrapperDiv
    {
        return new WrapperDiv(
            (new DescriptionList([
                'First name' => 'John',
                'Last name' => 'Doe',
                'Age' => '20',
            ]))->isHorizontal(true)
        );
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable|FieldUnavailable
     */
    public function wrappers(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/wrappers.md');

        $examples = new WrapperDiv([
            new H4Title('WrapperDiv Example'),
            $this->prepareWrapperDivExample()->addClass('mb-3 mt-3'),
            new H4Title('StringElement Example'),
            $this->prepareStringElementExample()->addClass('mb-3 mt-3'),
            new H4Title('WrapperSpan Example'),
            $this->prepareWrapperSpanExample()->addClass('mb-3 mt-3'),
            new H4Title('Complex Example'),
            $this->prepareWrappersComplexExample()
        ]);

        $dashboard->page()
            ->setPageTitle('Wrappers')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Examples')->content($examples);
        return $dashboard;
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareWrapperDivExample(): WrapperDiv
    {
        return (new WrapperDiv())
            ->attr('some', 'attr')
            ->attrs(['attr1' => 'val1', 'attr2' => 'val2'])
            ->addContent('Simple WrapperDiv')
            ->addClass('text-center border');
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareWrapperSpanExample(): WrapperDiv
    {
        return new WrapperDiv(
            (new WrapperSpan('Simple WrapperSpan'))
                ->addClass('simple_wrapper_span border')
                ->icon('fas fa-snowplow')
        );
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareStringElementExample(): WrapperDiv
    {
        return new WrapperDiv(
            (new StringElement('Simple StringElement'))
                ->class('simple_string_element border')
        );
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareWrappersComplexExample(): WrapperDiv
    {
        return (new WrapperDiv([
            (new WrapperDiv([
                (new WrapperDiv([
                    new WrapperDiv(
                        (new WrapperSpan('Left'))
                    ),
                    (new Image())->src(asset('scigeniq/dashboard/img/red_pill.png')),
                    (new WrapperSpan('hand'))->addClass('d-block')
                ]))->addClass('col text-danger')->addClass('text-center'),
                (new WrapperDiv([
                    new WrapperDiv(
                        (new WrapperSpan('Right'))
                    ),
                    (new Image())->src(asset('scigeniq/dashboard/img/blue_pill.png')),
                    (new WrapperSpan('hand'))->addClass('d-block')
                ]))->addClass('col text-primary')->addClass('text-center'),
            ]))->addClass('row'),
            (new WrapperDiv(
                (new StringElement('You take the blue pill... the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill... you stay in Wonderland, and I show you how deep the rabbit hole goes.'))
                    ->addClass('col-2 offset-5 font-italic')
            ))->addClass('row')
        ]))->addClass('border');
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function tabs(Dashboard $dashboard): Dashboard
    {
        $docs =  view()->file(__DIR__ . '/../../../docs/elements/tabs.md');

        $examples = $this->prepareTabsExamples();

        $dashboard->page()
            ->setPageTitle('Tabs')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Example')->content($examples);

        return $dashboard;
    }

    /**
     * @return mixed
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareTabsExamples(): Tabs
    {
        $image = (new Image('https://itfansoft.com/wp-content/uploads/2016/11/laravel.png'))->render();

        $tabs = (new Tabs())
            ->addTab()->title('Image')->content($image)->active()
            ->parent()->addTab()->title('Lorem Ipsum')->content($this->loremIpsum())
            ->parent();

        $navigation = $tabs->getNavigation()->addClass('bg-warning');

        $tabs->navigation($navigation);

        return $tabs;
    }

    /**
     * @return string
     */
    protected function loremIpsum(): string
    {
        return 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.';
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function grid(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/grid.md');

        $example = $this->prepareGridExample();

        $dashboard->page()
            ->setPageTitle('Grid')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Example')->content($example);

        return $dashboard;
    }

    /**
     * @param string $boxHeader
     * @return Box
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareBox(string $boxHeader = ''): Box
    {
        return (new Box())
            ->showFullscreenBtn()
            ->boxHeaderContent("<b>$boxHeader</b>")
            ->content([
                (new FormGroup())->select('outgoing_account_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Account', true),
                (new FormGroup())->dateInput('outgoing_date', now(), 'Date', true),
                (new FormGroup())->numberInput('value', 0, 'Value', true, 0.01),
                (new FormGroup())->numberInput('outgoing_commission', 0, 'Commission', false, 0.01),
                (new FormGroup())->select('outgoing_contractor_id', [1 => 'Option 1', 2 => 'Option 2'], '', 'Contractor'),
            ]);
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    protected function prepareGridExample(): WrapperDiv
    {
        $wrapper = new WrapperDiv();
        $wrapper->addContent([
            new H4Title('Grid with custom row count'),
            new WrapperSpan('For xs sizes set 1 column:'),
            (new Grid())->xsRowCount(1)->addContent([
                $this->prepareBox('Form 1'),
                $this->prepareBox('Form 2'),
                $this->prepareBox('Form 3'),
                $this->prepareBox('Form 4'),
            ]),
            new WrapperSpan('For sm sizes set 2 column:'),
            (new Grid())->smRowCount(2)->addContent([
                $this->prepareBox('Form 5'),
                $this->prepareBox('Form 6'),
                $this->prepareBox('Form 7'),
                $this->prepareBox('Form 8'),
            ]),
            new WrapperSpan('For md sizes set 3 column:'),
            (new Grid())->mdRowCount(3)->addContent([
                $this->prepareBox('Form 9'),
                $this->prepareBox('Form 10'),
                $this->prepareBox('Form 11'),
                $this->prepareBox('Form 12'),
            ]),
            new WrapperSpan('For lg sizes set 4 column:'),
            (new Grid())->lgRowCount(4)->addContent([
                $this->prepareBox('Form 13'),
                $this->prepareBox('Form 14'),
                $this->prepareBox('Form 15'),
                $this->prepareBox('Form 16'),
            ]),
            new H4Title('Grid with data before and after grid'),
            (new Grid())->lgRowCount(4)->addContent([
                $this->prepareBox('Form 17'),
                $this->prepareBox('Form 18'),
                $this->prepareBox('Form 19'),
                $this->prepareBox('Form 20'),
            ])
                ->beforeGrid('before grid')
                ->afterGrid('after grid'),
        ]);

        return $wrapper;
    }

    /**
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function links(Dashboard $dashboard): Dashboard
    {
        $docs = view()->file(__DIR__ . '/../../../docs/elements/links.md');

        $examples = $this->prepareLinksExamples();

        $dashboard->page()
            ->setPageTitle('Links')
            ->addElement()->tabs()->addTab()->title('Description')->content($docs)->active()
            ->parent()->addTab()->title('Example')->content($examples);

        return $dashboard;
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareLinksExamples(): WrapperDiv
    {
        return (new WrapperDiv([
            new H4Title('Link'),
            $this->prepareLinkExample()->addClass('mt-4 mb-4'),
            new H4Title('LinkButton'),
            $this->prepareLinkButtonExample()->addClass('mt-4 mb-4'),
            new H4Title('LinkJSDelete'),
            $this->prepareLinkJSDeleteExample()->addClass('mt-4 mb-4'),
        ]));
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareLinkExample(): WrapperDiv
    {
        return new WrapperDiv(
            (new Link('link to Google search'))
            ->link('https://www.google.com/')
            ->inNewTab()
        );
    }

    /**
     * @return WrapperDiv
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareLinkButtonExample(): WrapperDiv
    {
        return new WrapperDiv(
            (new LinkButton('link button to DuckDuckGo search'))
                ->link('https://duckduckgo.com/')
                ->class('btn-warning')
                ->icon('fa-link')
                ->inNewTab()
        );
    }

    /**
     * @return WrapperDiv
     * @throws NoOneFieldsWereDefined|FieldUnavailable
     */
    protected function prepareLinkJSDeleteExample(): WrapperDiv
    {
        return (new WrapperDiv([
            (new LinkJSDelete('Remove rectangle'))
                ->requestUri('/')
                ->method('GET')
                ->icon('fa-trash-alt')
                ->itemClass('rectangle')
                ->addClasses(' col-2 d-block btn-danger')
                ->setModalTitle('Rectangle deleting')
                ->setModalContent('Are you sure you want to delete Rectangle?'),
            (new WrapperDiv(
                (new WrapperDiv('Rectangle'))
                    ->addClass('w-25 h-100 border rectangle text-center float-right')
            ))->addClass('col-3 ')
        ]))->addClass('row');
    }
}
