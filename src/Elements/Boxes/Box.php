<?php


namespace Scigeniq\Dashboard\Elements\Boxes;

use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxTools($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxTools($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxBody($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxBody($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxFooter($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxFooter($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxBodyClasses($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxBodyClasses($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box boxHeaderContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addBoxHeaderContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box headerAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addHeaderAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box footerAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addFooterAvailable(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box colorType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addColorType($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box solid(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box addSolid(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Boxes\Box fullscreenBtn(bool $valueOrConfig)
 *
 ********************************************************************************************************************/

class Box extends ComplexElement
{
    protected $view = 'dashboard::elements.boxes.box';

    protected $available_fields = [
        'box_type',
        'box_title',
        'box_tools',
        'box_body',
        'box_footer',
        'class',
        'box_body_classes',
        'box_header_content',
        'header_available' => [
            'type' => 'bool',
            'default' => true
        ],
        'footer_available' => [
            'type' => 'bool',
            'default' => true
        ],
        'color_type' => [
            'default' => 'default',
            'acceptable_values' => [
                'default',
                'primary',
                'info',
                'warning',
                'success',
                'danger'
            ]
        ],
        'solid' => [
            'type' => 'bool',
            'default' => false
        ],
        'fullscreen_btn' => [
            'type' => 'bool',
            'default' => false
        ]
    ];

    protected $default_field = 'box_body';

    /**
     * @param string $link
     * @param string $label
     * @param string $iconClass
     * @param string $buttonClass
     *
     * @throws NoOneFieldsWereDefined
     */
    public function setToolsLinkButton(
        string $link,
        string $label = '',
        string $iconClass = '',
        string $buttonClass = 'btn-default'
    ) {
        $this->headerAvailable(true);

        $this->element('box_tools')
            ->linkButton()
            ->content($label)
            ->link($link)
            ->class($buttonClass)
            ->icon($iconClass);

        // add space for beauty view
        $this->addBoxTools(' ');
    }

    /**
     * Add link button to tools area
     *
     * @param string $link
     * @param string $label
     * @param string $iconClass
     * @param string $buttonClass
     *
     * @return $this
     * @throws NoOneFieldsWereDefined
     */
    public function addToolsLinkButton(
        string $link,
        string $label = '',
        string $iconClass = '',
        string $buttonClass = 'btn-default'
    ) {

        $this->headerAvailable(true);

        $this->addElement('box_tools')
            ->linkButton()
            ->content($label)
            ->link($link)
            ->class($buttonClass)
            ->icon($iconClass);

        // add space for beauty view
        $this->addBoxTools(' ');

        return $this;
    }

    /**
     * Simplify the box view
     */
    public function makeSimple()
    {
        return $this
            ->headerAvailable(false)
            ->footerAvailable(false);
    }

    /**
     * @param bool $state
     * @return Box
     */
    public function showFullscreenBtn(bool $state = true): Box
    {
        // if header_available = false but $state = true show header
        if (!$this->header_available && $state) {
            $this->headerAvailable($state);
        }

        return $this->fullscreenBtn($state);
    }
}
