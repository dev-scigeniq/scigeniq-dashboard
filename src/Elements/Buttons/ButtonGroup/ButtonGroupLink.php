<?php

namespace Scigeniq\Dashboard\Elements\Buttons\ButtonGroup;

use Scigeniq\Dashboard\Elements\Links\Link;

class ButtonGroupLink extends Link
{
    protected $view = 'dashboard::elements.buttons.button_group.button_group_link';

    protected $available_fields = [
        'content',
        'link',
        'classes'
    ];
}
