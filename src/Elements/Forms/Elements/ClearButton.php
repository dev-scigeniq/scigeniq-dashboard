<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;


use Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class ClearButton extends Input
{

    /**
     * Input constructor.
     *
     * @param null $content
     *
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        parent::__construct($content);

        $this->attr('type', 'reset');
    }
}
