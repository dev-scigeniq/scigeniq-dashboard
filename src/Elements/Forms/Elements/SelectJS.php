<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;



use Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsAvailable;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS multiple(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addMultiple(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS placeholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addPlaceholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS options(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addOptions(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS selectedKey($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addSelectedKey($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS selectedKeys(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addSelectedKeys(array $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS required(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\SelectJS addRequired(bool $valueOrConfig)
 *
 ********************************************************************************************************************/

class SelectJS extends Select implements MultifieldsAvailable
{
    /**
     * SelectJS constructor.
     *
     * @param null $content
     *
     * @throws NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        parent::__construct($content);

        $this->addClass('js-select2');
    }

    /**
     * Activate autocomplete mode
     *
     * @param string $requestUrl
     * @param string $placeholder
     *
     * @return $this
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function addAutocomplete(string $requestUrl, string $placeholder = 'Search')
    {
        // Remove old and add new class
        $this->replaceClass('js-select2', 'js-select2-ajax');

        $this->placeholder($placeholder);

        $this->attrs([
            'data-url' => $requestUrl
        ]);

        return $this;
    }
}
