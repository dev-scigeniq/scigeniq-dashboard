<?php


namespace Scigeniq\Dashboard\Elements\Forms\Elements;

use Scigeniq\Dashboard\Core\ComplexElement;
use Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Scigeniq\Dashboard\Elements\Forms\MultifieldsElements\MultifieldsAvailable;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea id($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addId($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea cols($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addCols($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea rows($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addRows($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea placeholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addPlaceholder($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea required(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addRequired(bool $valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Forms\Elements\Textarea addContent($valueOrConfig)
 *
 ********************************************************************************************************************/

class Textarea extends ComplexElement implements MultifieldsAvailable
{
    /** @var  string Component view name */
    protected $view = 'dashboard::elements.forms.elements.textarea';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'id',
        'class' => [
            'default' => 'form-control'
        ],
        'name',
        'cols' => [
            'default' => '50'
        ],
        'rows' => [
            'default' => '10'
        ],
        'title',
        'placeholder',
        'required'=> [
//            'type' => 'bool',
            'default' => false,
        ],
        'content'
    ];

    /** @var  string Default section for current component */
    protected $default_field = 'content';

    /**
     * Input constructor.
     *
     * @param null $content
     *
     * @throws NoOneFieldsWereDefined
     */
    public function __construct($content = null)
    {
        parent::__construct($content);

        $this->id = uniqid();
    }

    /**
     * Name field validation
     *
     * @param $value
     * @return bool
     */
    public function isValidNameFieldValue($value)
    {
        return strlen($value) > 0;
    }
}
