<?php


namespace Scigeniq\Dashboard\Elements\Icons;


use Scigeniq\Dashboard\Core\ComplexElement;


/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon icon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon addIcon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon classes($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Icons\Icon addClasses($valueOrConfig)
 *
 ********************************************************************************************************************/

class Icon extends ComplexElement
{
    protected $view = 'dashboard::elements.icons.icon';

    protected $available_fields = [
        'icon',
        'title',
        'classes'
    ];

    protected $default_field = 'icon';

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed|Icon
     * @throws \Scigeniq\Dashboard\Core\Content\Exceptions\FieldUnavailable
     * @throws \Scigeniq\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function __call($name, $arguments)
    {
        if(strpos($name, 'fa') === 0){
            return $this->icon(str_replace('_', '-', snake_case($name)));
        }

        return parent::__call($name, $arguments);
    }


}
