<?php


namespace Scigeniq\Dashboard\Elements\Links;

use Scigeniq\Dashboard\Core\Content\JsActionsApplicable;
use Scigeniq\Dashboard\JsActions\JsActionsCollection;
use Scigeniq\Dashboard\JsActions\ModalActions;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton addContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton link($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton addLink($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton addClass($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton icon($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Links\LinkButton addIcon($valueOrConfig)
 *
 ********************************************************************************************************************/

class LinkButton extends Link implements JsActionsApplicable
{
    protected $view = 'dashboard::elements.links.link_button';

    protected $available_fields = [
        'content',
        'link',
        'class' => [
          'default' => 'btn-info'
        ],
        'icon'
    ];

    protected $default_field = 'content';
}
