<?php


namespace Scigeniq\Dashboard\Elements;


use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\ProductBlock name($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\ProductBlock addName($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\ProductBlock img($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\ProductBlock addImg($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\ProductBlock link($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\ProductBlock addLink($valueOrConfig)
 *
 ********************************************************************************************************************/

class ProductBlock extends ComplexElement
{
    /** @var  StringElement Component view name */
    protected $view = 'dashboard::elements.product_block';

    /** @var  array Sections available in page */
    protected $available_fields = [
        'name',
        'img',
        'link'
    ];

    /** @var  StringElement Default section for current component */
    protected $default_field = 'name';
}
