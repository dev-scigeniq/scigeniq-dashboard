<?php


namespace Scigeniq\Dashboard\Elements;


use Scigeniq\Dashboard\Core\ComplexElement;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\StringElement content($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\StringElement addContent($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\StringElement class($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\StringElement addClass($valueOrConfig)
 *
 ********************************************************************************************************************/

class StringElement extends ComplexElement
{
    protected $view = 'dashboard::elements.string_element';

    protected $available_fields = [
        'content',
        'class'
    ];

    protected $default_field = 'content';
}
