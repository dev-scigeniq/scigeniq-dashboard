<?php


namespace Scigeniq\Dashboard\Elements\Titles;

/*********************************************************************************************************************
 * Generated meta methods
 *********************************************************************************************************************
 *
 * @method \Scigeniq\Dashboard\Elements\Titles\H4Title title($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H4Title addTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H4Title subTitle($valueOrConfig)
 * @method \Scigeniq\Dashboard\Elements\Titles\H4Title addSubTitle($valueOrConfig)
 *
 ********************************************************************************************************************/

class H4Title extends H1Title
{
    protected $view = 'dashboard::elements.titles.h4_title';
}
