<?php


namespace Scigeniq\Dashboard\JsActions;

use Illuminate\Support\Str;
use Scigeniq\Dashboard\Core\Content\JsActionsApplicable;

class HideShowOnClick extends JsActionApplicator
{
    /**
     * @param string $targetBlockClass
     * @param bool $showTargetBlock
     * @return JsActionsApplicable
     */
    public function makeHideShowController(
        string $targetBlockClass,
        bool $showTargetBlock = true
    ): JsActionsApplicable {

        $this->element
            ->addClass('js_toggle-btn')
            ->attr('data-name-blk', Str::start($targetBlockClass, '.'));

        if ($showTargetBlock) {
            $this->element->attr('data-show-blk', '');
        }

        return $this->element;
    }
}
