<?php


namespace Scigeniq\Dashboard\JsActions;


use Scigeniq\Dashboard\Core\Content\JsActionsApplicable;

abstract class JsActionApplicator
{
    /** @var JsActionsApplicable */
    protected $element;

    /**
     * JsActionApplicator constructor.
     *
     * @param JsActionsApplicable $element
     */
    public function __construct(JsActionsApplicable $element)
    {
        $this->element = $element;
    }
}
